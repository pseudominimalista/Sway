#!/bin/sh
yay -Syu brightnessctl google-chrome pulsemixer vim foot blueman mako wf-recorder wl-clipboard wlsunset autotiling sway swaybg swayidle swaylock sway-systemd swaytools swayhide grim slurp ttf-jetbrains-mono adobe-source-han-sans-jp-fonts noto-fonts-emoji xdg-desktop-portal-wlr rofi xorg-xwayland vscodium-bin thunar tumbler libreoffice-fresh mupdf file-roller p7zip wine pulsemixer nordic-darker-theme nordic-wallpapers-git gvfs gvfs-mtp gvfs-smb xdg-utils nordzy-cursors imv cmus mpv yt-dlp xorg-xwayland &&

xdg-user-dirs-update --force
cp -r .local ~/
cp -r .config/ ~/
cp .bash* ~/
cp .wp ~/

chmod +x ~/.local/bin/*
